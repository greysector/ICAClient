%global disp_ver 19.6.0
%global change_no 60
%global port linuxx64
# Tweak to have debuginfo
%undefine _missing_build_ids_terminate_build
%undefine _debugsource_packages
%undefine _unique_build_ids
%global _no_recompute_build_ids 1
%global __provides_exclude_from ^%{_libdir}/ICAClient/(lib/)?.*\\.so$
%global __requires_exclude ^libAnalyticsInterfacePd\\.so.*$

Summary: Client software that provides access to XenDesktop and XenApp installations
Name: ICAClient
Version: %{disp_ver}.%{change_no}
Release: 1
URL: https://www.citrix.com/products/workspace-app/
# Direct URLs don't work anymore, go to
# https://www.citrix.com/downloads/workspace-app/linux/workspace-app-for-linux-latest.html
Source0: linuxx64-%{version}.tar.gz
Source3: wfica.sh
Source4: wfica_assoc.sh
Source5: copyright.txt
Source6: https://docs.citrix.com/en-us/citrix-workspace-app-for-linux/citrix-workspace-app-for-linux.pdf
Source10: configmgr.desktop
Source11: conncenter.desktop
Source12: new_store.desktop
Source13: selfservice.desktop
Source14: wfica.desktop
Source20: Class3PCA_G5_v3.pem
Source21: Class3SGC_CA_v3.pem
Source22: DigiCertHAEVRootCA.pem
License: Citrix
ExclusiveArch: x86_64
BuildRequires: chrpath
BuildRequires: desktop-file-utils
BuildRequires: %{_bindir}/execstack
Recommends: alsa-plugins-pulseaudio%{_isa}
Recommends: gstreamer1-libav%{_isa}
Recommends: gstreamer1-plugins-bad-free%{_isa}
Recommends: gstreamer1-plugins-good%{_isa}
Recommends: gtk2-engines%{_isa}
Recommends: opensc%{_isa}
Recommends: pcsc-lite-libs%{_isa}
Recommends: webkit2gtk3%{_isa}
# bundled in libAnalyticsInterfacePd.so
Provides: bundled(curl) = 7.53.0
# bundled js libraries in site/scripts
Provides: bundled(js-jquery1) = 1.7.1
Provides: bundled(js-jquery-ui) = 1.8.16
Provides: bundled(js-jquery-hammer) = 1.0.0
Provides: bundled(js-jquery-jscrollpane) = 2.0.0beta11
Provides: bundled(js-jquery-mousewheel) = 3.0.6

%description
Citrix Receiver for Linux is a new lightweight software client that makes
accessing virtual applications and desktops on any device as easy as turning on
your TV.

Much like a satellite or cable TV receiver in a broadcast media service, Citrix
Receiver allows IT organizations to deliver desktops and applications as an
on-demand service to any device in any location with a rich "high definition"
experience.

As long as employees have Citrix Receiver installed, IT no longer has
to worry about whether they are delivering to a PC in the office or a Mac
at home. This approach radically simplifies desktop management for IT and gives
end users far more flexibility and independence in how and where they work.

%prep
%setup -qc
cp -p %{SOURCE6} .
pushd %{port}/%{port}.cor
chmod -x config/AuthManConfig.xml icons/*.png
chmod +x lib/libcoreavc_sdk.so
# remove unused script
rm util/{ctx_app_bind,hinst,icalicense.sh}
# introduces missing Requires: libjpeg.so.8
rm lib/ctxjpeg_fb_8.so
chrpath -d selfservice
chrpath -d lib/libkcpm.so VDMSSPI.DLL

# clear executable stack bit
execstack -c lib/libcoreavc_sdk.so
# fix pscs-lite dependency and enable Multimedia virtual channel
find nls -name module.ini | xargs sed -i -e 's/libpcsclite.so/libpcsclite.so.1/ ; s/^MultiMedia=Off$/MultiMedia=On/'
sed -i -e "s/libpcsclite.so/libpcsclite.so.1/ ; s,/opt/Citrix/ICAClient,%{_libdir}/ICAClient," util/hdxcheck.sh

# linked against libcrypto.so.1.0.0, won't run
rm util/ctxwebhelper

# disable CEIP by default
# https://docs.citrix.com/en-us/citrix-workspace-app-for-linux/install/configure-ceip.html
sed -i -e "/EnableCeip/ s/Enable/Disable/g" nls/*/module.ini
popd

%build

%install
pushd %{port}/%{port}.cor
mkdir -p %{buildroot}{%{_libdir}/{ICAClient/{lib,pkginf},gstreamer-1.0},/etc/icalicense}
cp -pr PKCS#11 config gtk help icons keyboard keystore nls util \
  %{buildroot}%{_libdir}/ICAClient/
cp -p *.DLL lib*.so AuthManagerDaemon PrimaryAuthManager \
  ServiceRecord wfica \
  %{buildroot}%{_libdir}/ICAClient/
cp -p lib/{ctx{h264,jpeg}_fb,lib{AMSDK,coreavc_sdk,kcp{h,m}},UIDialogLib}.so \
  %{buildroot}%{_libdir}/ICAClient/lib/
cp -pr site selfservice %{buildroot}%{_libdir}/ICAClient/
cp -pr lib/{SelfService_ext,UIDialogLibWebKit3{_ext,.so}} %{buildroot}%{_libdir}/ICAClient/lib/
desktop-file-install --vendor "" --dir %{buildroot}%{_datadir}/applications %{SOURCE13}
sed -i -e 's,@libdir@,%{_libdir},g' %{buildroot}%{_datadir}/applications/$(basename %{SOURCE13})

install -Dpm644 desktop/Citrix-mime_types.xml %{buildroot}%{_datadir}/mime/packages/Citrix-mime_types.xml
install -Dpm644 icons/receiver.png %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/citrix-manager.png
install -Dpm644 icons/000_Receiver_64.png %{buildroot}%{_datadir}/icons/hicolor/64x64/apps/citrix-manager.png
install -Dpm644 icons/manager.png %{buildroot}%{_datadir}/icons/hicolor/256x256/apps/citrix-manager.png
popd

pushd %{buildroot}%{_libdir}/ICAClient

pushd nls
for e in * ; do
  ln -s XCapture.ad $e/XCapture
done
ln -s en C
for e in de en es fr ja ; do
  ln -s $e.UTF-8 $e.utf8
  ln -s $e.UTF-8 $e.UTF8
done
for e in zh_CN zh_SN ; do
  ln -s zh_HANS $e
  ln -s zh_HANS.UTF-8 $e.utf8
  ln -s zh_HANS.UTF-8 $e.UTF8
  ln -s zh_HANS.UTF-8 $e.UTF-8
done
popd

install -pm644 %{SOURCE5} nls/en/
ln -s nls/en/copyright.txt ./
ln -s nls/en.UTF-8/eula.txt ./
ln -s ../nls/en/appsrv.template config/
ln -s ../nls/en/module.ini config/
ln -s ../nls/en/wfclient.template config/
ln -s ../nls/en/index.htm help/

install -pm644 %{S:20} %{S:21} %{S:22} ./keystore/cacerts/

pushd util
mv libgstflatstm1.0.so %{buildroot}%{_libdir}/gstreamer-1.0/libgstflatstm.so
rm gst_aud_{play,read} gst_{play,read}0.10 libgstflatstm0.10.so
ln -s gst_play1.0 gst_play
ln -s gst_read1.0 gst_read
ln -s pnabrowse pnabrowse_launch
./ctx_rehash
popd

touch config/.server

touch pkginf/F.core.%{port}

cat << __END__ > pkginf/Ver.core.%{port}
ID_VERSION=%{version}
DISP_VERSION=%{disp_ver}
CHANGE_NO=%{change_no}
ID_PRODUCT_NAME=CitrixICAClient
DISP_PRODUCT_NAME=Citrix Receiver for Linux
uname=$(uname -a)
PORT=%{port}
LANGUAGE=*
date=$(date '+%Y:%m:%d:%H:%M:%S %Z' | sed 's/://g')
INFO_FILE=RPM_installation
DATA_FILE=RPM_package
BROWSER_DIR=%{_libdir}/mozilla/plugins
__END__

echo %{change_no} > pkginf/changeno.dat

for f in %{SOURCE3} %{SOURCE4} ; do
  install -pm755 $f ./
  sed -i -e 's,@libdir@,%{_libdir},g' $(basename $f)
done
popd
# %{buildroot}%{_libdir}/ICAClient

for f in %{SOURCE10} %{SOURCE11} %{SOURCE12} %{SOURCE14} ; do
  desktop-file-install --vendor "" --dir %{buildroot}%{_datadir}/applications $f
  sed -i -e 's,@libdir@,%{_libdir},g' %{buildroot}%{_datadir}/applications/$(basename $f)
done

# generate localized files list
find %{buildroot} -type f -o -type l|sed '
s:'"%{buildroot}"'::
s:\(.*/nls/\)\([^/_]\+\)\(.*\.mo$\):%lang(\2) \1\2\3:
s:\(.*/nls/\)\([^/_]\+\)\(.*\.nls$\):%lang(\2) \1\2\3:
s:\(.*/nls/\)\([^/_]\+\)\(XCapture.*\):%lang(\2) \1\2\3:
s:^\([^%].*\)::
s:%lang(C) ::
/^$/d' > %{name}.lang

%post
if [ ! -f /etc/icalicense/clientlicense ]; then
  %{_libdir}/ICAClient/util/echo_cmd -l > /etc/icalicense/clientlicense
fi

%files -f %{name}.lang
%doc citrix-workspace-app-for-linux.pdf
%dir /etc/icalicense
%ghost %attr(644,root,root) %config(noreplace) /etc/icalicense/clientlicense
%{_libdir}/ICAClient
%exclude %{_libdir}/ICAClient/nls/*/LC_MESSAGES/*.mo
%exclude %{_libdir}/ICAClient/nls/*/pna.nls
%exclude %{_libdir}/ICAClient/nls/*/XCapture*
%{_libdir}/gstreamer-1.0/libgstflatstm.so
%{_datadir}/applications/configmgr.desktop
%{_datadir}/applications/conncenter.desktop
%{_datadir}/applications/new_store.desktop
%{_datadir}/applications/selfservice.desktop
%{_datadir}/applications/wfica.desktop
%{_datadir}/icons/hicolor/32x32/apps/citrix-manager.png
%{_datadir}/icons/hicolor/64x64/apps/citrix-manager.png
%{_datadir}/icons/hicolor/256x256/apps/citrix-manager.png
%{_datadir}/mime/packages/Citrix-mime_types.xml

%changelog
* Tue Jul 23 2019 Dominik Mierzejewski <rpm@greysector.net> - 19.6.0.60-1
- update to 19.6.0.60
- drop support for non-x86_64

* Mon Jun 10 2019 Dominik Mierzejewski <rpm@greysector.net> - 19.6.0.6-1
- update to 19.6.0.6 Early Access Release
- include selfservice again, it was ported to modern WebKit

* Tue Nov 06 2018 Dominik Mierzejewski <rpm@greysector.net> - 18.10.0.11-1
- update to 18.10.0.11
- rename to Citrix Workspace where applicable
- disable phoning home (Customer Experience Improvement Program)

* Thu Nov 01 2018 Dominik Mierzejewski <rpm@greysector.net> - 13.10.0.20-1
- update to 13.10.0.20

* Mon Apr 30 2018 Dominik Mierzejewski <rpm@greysector.net> - 13.9.1.6-2
- drop UIDialogLibWebKit.so on F27+ (depends on old webkitgtk)
- ship gst_play and gst_read symlinks

* Sun Mar 25 2018 Dominik Mierzejewski <rpm@greysector.net> - 13.9.1.6-1
- update to 13.9.1.6

* Thu Mar 22 2018 Dominik Mierzejewski <rpm@greysector.net> - 13.9.0.102-1
- update to 13.9.0.102
- switch to gstreamer-1.0 and drop gstreamer-0.10 support
- drop obsolete scriptlets
- don't package Browser Content Redirection driver on RHEL due to old libstdc++

* Wed Feb 28 2018 Dominik Mierzejewski <rpm@greysector.net> - 13.4.0.10109380-8
- add DigiCert High Assurance EV Root CA certificate

* Mon Nov 27 2017 Dominik Mierzejewski <rpm@greysector.net> - 13.4.0.10109380-7
- drop selfservice binary in F27+ due to retired webkitgtk
- work around missing build-id for F27+

* Tue Jul 11 2017 Dominik Mierzejewski <rpm@greysector.net> - 13.4.0.10109380-6
- fix single/double dash before icaroot option confusion
- drop Mozilla plugin package (NPAPI no longer supported by Firefox)

* Fri Apr 28 2017 Dominik Mierzejewski <rpm@greysector.net> - 13.4.0.10109380-5
- support building for EL7 (no weak deps)

* Fri Apr 14 2017 Dominik Mierzejewski <rpm@greysector.net> - 13.4.0.10109380-4
- document bundled JavaScript libraries
- add weak dependency on flash plugin for flash redirection support
- improve debuginfo tweak on i686
- use included ctx_rehash instead of system c_rehash

* Fri Sep 23 2016 Dominik Mierzejewski <rpm@greysector.net> - 13.4.0.10109380-3
- fix broken links to ini file templates
- make webkitgtk a weak dependency, only used by selfservice
- add a soft dependency on gtk2-engines

* Thu Sep 08 2016 Dominik Mierzejewski <rpm@greysector.net> - 13.4.0.10109380-2
- add weak dependency on opensc for enhanced smart card support
- drop redundant ? from _isa macro usage

* Wed Sep 07 2016 Dominik Mierzejewski <rpm@greysector.net> - 13.4.0.10109380-1
- update to 13.4.0.10109380
- fix rpath removal
- use c_rehash instead of calling openssl manually

* Fri May 13 2016 Dominik Mierzejewski <rpm@greysector.net> - 13.3.0.344519-3
- add OEM doc
- recommend alsa-plugins-pulseaudio (no sound otherwise)

* Fri Feb 19 2016 Dominik Mierzejewski <rpm@greysector.net> - 13.3.0.344519-2
- include copyright.txt from upstream
- replicate symlinks from upstream package
- don't ship hinst script
- mark localized files as such
- fix build on i686

* Wed Jan 20 2016 Dominik Mierzejewski <rpm@greysector.net> - 13.3.0.344519-1
- update to 13.3.0.344519
- add 32x32 and 64x64 icons

* Wed Sep 30 2015 Dominik Mierzejewski <rpm@greysector.net> - 13.2.1.328635-1
- update to 13.2.1.328635
- fix versioning in changelog
- don't expose private libraries in Provides:
- remove unused script
- enable (partial) debuginfo package
- clear executable stack bit from libcoreavc_sdk.so

* Tue Aug 25 2015 Dominik Mierzejewski <rpm@greysector.net> - 13.2.0.322243-2
- add arm build
- fix permissions on files
- add optional dependencies
- filter out Provides: from private libraries

* Mon Aug 24 2015 Dominik Mierzejewski <rpm@greysector.net> - 13.2.0.322243-1
- updated to 13.2.0.322243
- fix up ICAClient root dir in shell scripts
- drop obsolete manual Requires

* Fri Nov 21 2014 Dominik Mierzejewski <rpm@greysector.net> - 13.1.285639-1
- initial build
